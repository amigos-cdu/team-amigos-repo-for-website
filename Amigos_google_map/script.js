/*This displays a marker at darwin of Australia where our domain called as "Amigos"
restaurant is located*/
/*When the user clicks the marker, an info window opens depicting some basic information 
about our restaurant */

function initMap() {
    const darwin = { lat: -12.4629, lng: 130.8449 };
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 14,
      center: darwin,
    });
    const contentString = "<p><b>Amigos</b><br> The one and only Authentic Mexican Food in the town.</p>"
    const infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 200,
    });
    const marker = new google.maps.Marker({
      position: darwin,
      map,
      title: "darwin (Amigos)",
    });
    marker.addListener("click", () => {
      infowindow.open(map, marker);
    });
  };